package com.createaccount;

import org.openqa.selenium.By;
import com.wrapper.Elements;

public class CreateAccount {

	private static final By CREATE_ACCOUNT_BUTTON_LOCATOR = By.xpath("//a[@class='_42ft _4jy0 _6lti _4jy6 _4jy2 selected _51sy']");
	private static final By FIRSTNAME_TEXTBOX_LOCATOR = By.name("firstname");
	private static final By LASTNAME_TEXTBOX_LOCATOR = By.name("lastname");
	private static final By EMAIL_TEXTBOX_LOCATOR = By.name("reg_email__");
	private static final By REEMAIL_TEXTBOX_LOCATOR = By.name("reg_email_confirmation__");
	private static final By ENTERPASSWORD_TEXTBOX_LOCATOR = By.name("reg_passwd__");
	private static final By DAY_DROPDOWN_LOCATOR = By.name("birthday_day");
	private static final By MONTH_DROPDOWN_LOCATOR = By.name("birthday_month");
	private static final By YEAR_DROPDOWN_LOCATOR = By.name("birthday_year");
	private static final By MALE_RADIOBUTTON_LOCATOR = By.xpath("//input[@value='2']");
	private static final By FEMALE_RADIOBUTTON_LOCATOR = By.xpath("//input[@value='1']");
	private static final By SUBMIT_BUTTON_LOCATOR = By.name("websubmit");

	
	public static void createUserAccount(String firstName, String lastName, String emailId, String password, String dayOfBirth, String monthOfBirth, String yearOfBirth, String Sex) throws InterruptedException
	{
		Elements.clickOnElement(CREATE_ACCOUNT_BUTTON_LOCATOR);
		Thread.sleep(5000);
		Elements.typeText(FIRSTNAME_TEXTBOX_LOCATOR, firstName);
		Elements.typeText(LASTNAME_TEXTBOX_LOCATOR, lastName);
		Elements.typeText(EMAIL_TEXTBOX_LOCATOR, emailId);
		Elements.typeText(REEMAIL_TEXTBOX_LOCATOR, emailId);
		Elements.typeText(ENTERPASSWORD_TEXTBOX_LOCATOR, password);
		Elements.selectFromDropdown(dayOfBirth, DAY_DROPDOWN_LOCATOR);
		Elements.selectFromDropdown(11, MONTH_DROPDOWN_LOCATOR);
		Elements.selectFromDropdown(yearOfBirth, YEAR_DROPDOWN_LOCATOR);
		
		if (Sex.equalsIgnoreCase("M"))
		{
			Elements.clickOnElement(MALE_RADIOBUTTON_LOCATOR);
		}
		else if (Sex.equalsIgnoreCase("F"))
		{
			Elements.clickOnElement(FEMALE_RADIOBUTTON_LOCATOR);
		}
		Elements.clickOnElement(SUBMIT_BUTTON_LOCATOR);
	}
}
