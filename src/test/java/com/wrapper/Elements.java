package com.wrapper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Elements {
	private static WebDriver wd;
	private static Select select;
	
	public static void launchBrowser(String browser)
	{
		if(browser.equalsIgnoreCase("Chrome"))
		{
			WebDriverManager.chromedriver().setup();
			wd = new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("Firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			wd = new ChromeDriver();
		}
		else 
		{
			System.out.println("Select Chrome or FireFox as browser");
		}
	}

	public static void clickOnElement(By elementLocator)
	{
		WebElement element = wd.findElement(elementLocator);
		element.click();
	}
	
	public static void typeText(By elementLocator, String textToEnter)
	{
		WebElement element = wd.findElement(elementLocator);
		element.sendKeys(textToEnter);
	}

	public static void selectFromDropdown(By dropdownLocator, String value)
	{
		WebElement element = wd.findElement(dropdownLocator);
		select = new Select(element);
		select.selectByValue(value);
	}
	
	public static void selectFromDropdown(String visibleText, By dropdownLocator)
	{
		WebElement element = wd.findElement(dropdownLocator);
		select = new Select(element);
		select.selectByVisibleText(visibleText);
	}
	
	public static void selectFromDropdown(int indexText, By dropdownLocator)
	{
		WebElement element = wd.findElement(dropdownLocator);
		select = new Select(element);
		select.selectByIndex(indexText);
	}
	
	public static void loadPage(String url)
	{
		wd.get(url);
	}
}
